<html>

<head>
    <title>Comics for {{ today }}</title>
</head>

<body bgcolor="#ffffff" text="#000000" link="#ff00ff">

<a name="top">

<center>
    <font face="helvetica" size="+2"><b><u>Comics for {{ today }}</u></b></font>
</center>

<p><font face="helvetica">
&lt; {% if yesterday is defined and yesterday != None %}<!--yesterday="{{ yesterday }}"--><a href="{{ yesterday }}">Previous day</a>{% endif %}<!--nextday--> | <a href="archive.html">Archives</a> &gt;
</font></p>
<font face="helvetica">Strips:</font><br>
{% for comic in comics %}
    <a href="#{{ comic }}">{{ comicdata[comic].definition.name }}</a>{% if not loop.last %}&nbsp;&nbsp;{% endif %}
{% endfor %}
<br><br>

<table border="0">
{% for comic in comics %}
    <tr><td><font face="helvetica" size="+1"><b><a name="{{ comic }}" href="{{ comicdata[comic].definition.homepage|d('') }}">{{ comicdata[comic].definition.name }}</a>
{%   if comicdata[comic].definition.artist is defined %}
by {{ comicdata[comic].definition.artist }}
{%   endif %}
</b></font></td></tr>
    <tr>
      <td>
{%   if 'error' in comicdata[comic]: %}
        [{{ comicdata[comic].error }}]
{%   else %}
        <img src="../{{ comicdata[comic].file }}" alt="{{ comicdata[comic].definition.name }}" style="max-width: 100%;"><br><a href="#top">Return to top</a>
{%   endif %}
        <p>&nbsp;</p>
      </td>
    </tr>
{% endfor %}

</table>

<p><font face="helvetica">
&lt; {% if yesterday is defined and yesterday != None %}<a href="{{ yesterday }}">Previous day</a>{% endif %}<!--nextday--> &gt;
</font></p>

<font face="helvetica">Generated at {{ now }}</font>

</body>

</html>
