FROM python:3.11-alpine

WORKDIR /app
COPY requirements.txt comics.py comics.yml ./
COPY views/* views/
RUN pip install -r requirements.txt

COPY crontab /var/spool/cron/crontabs/root

VOLUME /data
CMD crond -f
