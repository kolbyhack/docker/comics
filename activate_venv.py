#!/usr/bin/env python3

import os
import sys

def activate(path):
    if hasattr(sys, 'real_prefix'):
        return

    base = os.path.abspath(path)
    os.environ['PATH'] = '%s%s%s' % (os.path.join(base, 'bin'), os.pathsep, os.environ.get('PATH', ''))
    if sys.platform == 'win32':
        site_packages = os.path.join(base, 'Lib', 'site-packages')
    else:
        site_packages = os.path.join(base, 'lib', 'python%s' % sys.version[:3], 'site-packages')
    prev_sys_path = list(sys.path)
    import site
    site.addsitedir(site_packages)
    sys.real_prefix = sys.prefix
    sys.prefix = base
    # Move the added items to the front of the path:
    new_sys_path = []
    for item in list(sys.path):
        if item not in prev_sys_path:
            new_sys_path.append(item)
            sys.path.remove(item)
    sys.path[:0] = new_sys_path

